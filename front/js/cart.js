// récupérer les éléments du LS et compléter la liste des éléments depuis l'API
const section = document.getElementById("cart__items")
const totalPriceElm = document.getElementById("totalPrice")
const totalItemElm = document.getElementById("totalQuantity")



// Fonction qui va permettre l'affichage du panier avec la création et l'intégration des éléments du DOM. 
const displayCart = () => {
    // on récupère les données du localStorage
    const lsCart = getCart()

    // on trie les ID des produits afin de regrouper les produits par ID
    lsCart.sort((a, b) => (a._id > b._id) ? 1 : (a._id < b._id) ? -1 : 0)


    let totalPrice = 0
    let totalItem = 0

    // on créé une boucle qui, pour chaque produit du panier, va créer les éléments du DOM de manière dynamique
    lsCart.forEach(product => {
        const article = document.createElement("article")
        article.classList.add("cart__item")
        article.dataset.id
        article.dataset.color

        const divImg = document.createElement("div")
        divImg.classList.add("cart__item__img")
        const img = document.createElement("img")


        const divContent = document.createElement("div")
        divContent.classList.add("cart__item__content")

        const divContentDesc = document.createElement("div")
        divContentDesc.classList.add("cart__item__content__description")
        const h2 = document.createElement("h2")

        const pColor = document.createElement("p")
        pColor.innerText = product.color
        const pPrice = document.createElement("p")


        const divContentSettings = document.createElement("div")
        divContentSettings.classList.add("cart__item__content__settings")
        const divContentSettingsQuantity = document.createElement("div")
        divContentSettingsQuantity.classList.add("cart__item__content__settings__quantity")
        const pQty = document.createElement("p")
        pQty.innerText = "Qté : "
        const inputQty = document.createElement("input")
        inputQty.classList.add("itemQuantity")
        inputQty.value = product.quantity
        inputQty.setAttribute("type", "number")
        inputQty.setAttribute("min", 1)
        inputQty.setAttribute("max", 100)

        // on créé un évent qui va modifier la quantité en temps réel. On va choisir une nouvelle quantité, vider le champs, afficher la nouvelle quantité en rechargant le DOM.
        inputQty.addEventListener("change", (e) => {
            changeQuantity(lsCart, product, parseInt(e.currentTarget.value))
            section.innerHTML = ""
            displayCart()
        })

        // sur l'event du clique sur le boutton supprimer, on execute la fonction permettant d'enlever le produit du LS, on vide les éléments et on recharge tout le panier
        const divContentSettingsDelete = document.createElement("div")
        divContentSettingsDelete.classList.add("cart__item__content__settings__delete")
        const pDelete = document.createElement("p")
        pDelete.textContent = "Supprimer"
        pDelete.addEventListener("click", () => {
            removeFromCart(product)
            section.innerHTML = ""
            totalItemElm.innerText = 0
            totalPriceElm.innerText = 0
            displayCart()
        })


        // on met en place l'ordre d'imbrication des éléments du DOM
        section.append(article)
        article.append(divImg)
        divImg.append(img)

        article.append(divContent)
        divContent.append(divContentDesc)
        divContentDesc.append(h2)
        divContentDesc.append(pColor)
        divContentDesc.append(pPrice)

        divContent.append(divContentSettings)
        divContentSettings.append(divContentSettingsQuantity)
        divContentSettingsQuantity.append(pQty)
        divContentSettingsQuantity.append(inputQty)

        divContentSettings.append(divContentSettingsDelete)
        divContentSettingsDelete.append(pDelete)


        // on met a jour la quantité totale
        totalItem += product.quantity
        totalItemElm.innerText = totalItem

        // vu qu'on a pas toutes les données dans le LS, on fait un fetch afin d'aller récupèrer le reste des données
        fetch(`http://localhost:3000/api/products/${product._id}`)
            .then(response => response.json())
            .then(productDetails => {
                img.src = productDetails.imageUrl
                img.alt = productDetails.altTxt
                h2.innerText = productDetails.name
                pPrice.innerText = `${productDetails.price},00 €`

                // on met a jour le prix total
                totalPrice += product.quantity * productDetails.price
                totalPriceElm.innerText = `${totalPrice},00`

            })

    })
}
displayCart()



// /////////////////////////////////////////////////////////////////////////////
// // ******************* FORMULAIRE *******************

let form = document.querySelector(".cart__order__form")

// ecouter la modification du prénom
form.firstName.addEventListener('change', function () {
    validFirstName(this)
})

// ecouter la modification du nom
form.lastName.addEventListener('change', function () {
    validLastName(this)
})

// ecouter la modification de l'adresse
form.address.addEventListener('change', function () {
    validAddress(this)
})

// ecouter la modification de la ville
form.city.addEventListener('change', function () {
    validCity(this)
})

// ecouter la modification de l'email
form.email.addEventListener('change', function () {
    validEmail(this)
})

// ************** VALIDATION PRENOM **************
const validFirstName = () => {
    const inputFirstName = form.firstName
    // création de la reg exp pour la validation du prénom
    let firstNameRegExp = new RegExp('^[a-zA-Z-\é\è\ê\ï\î]+$', 'g')
    let testFirstName = firstNameRegExp.test(inputFirstName.value)

    // Récupération de la balise suivant l'input, soit le "p" pour y afficher le message
    let errorMsg = inputFirstName.nextElementSibling

    // on test le reg exp
    if (testFirstName) {
        errorMsg.innerHTML = "Saisie valide"
        errorMsg.style.color = "rgb(0, 220, 0)"
    } else {
        errorMsg.innerHTML = "Saisie non valide. Caractère non autorisé."
        errorMsg.style.color = "rgb(180, 0, 0)"
    }
    return testFirstName
}

// ************** VALIDATION NAME **************
const validLastName = () => {
    const inputLastName = form.lastName
    // création de la reg exp pour la validation du nom
    let lastNameRegExp = new RegExp('^[a-zA-Z- \']+$', 'g')
    let testLastName = lastNameRegExp.test(inputLastName.value)

    // Récupération de la balise suivant l'input, soit le "p" pour y afficher le message
    let errorMsg = inputLastName.nextElementSibling

    // on test le reg exp
    if (testLastName) {
        errorMsg.innerHTML = "Saisie valide"
        errorMsg.style.color = "rgb(0, 220, 0)"
    } else {
        errorMsg.innerHTML = "Saisie non valide. Caractère non autorisé."
        errorMsg.style.color = "rgb(180, 0, 0)"
    }
    return testLastName
}

// ************** VALIDATION ADRESSE **************
const validAddress = () => {
    const inputAddress = form.address

    // création de la reg exp pour la validation de l'adresse
    let addressRegExp = new RegExp("(^[0-9]{1,5})+([a-zA-Z0-9-' \é\è\ê\ï\î])+$", "g")
    let testAddress = addressRegExp.test(inputAddress.value)

    // Récupération de la balise suivant l'input, soit le "p" pour y afficher le message
    let errorMsg = inputAddress.nextElementSibling

    // on test le reg exp
    if (testAddress) {
        errorMsg.innerHTML = "Saisie valide"
        errorMsg.style.color = "rgb(0, 220, 0)"
    } else {
        errorMsg.innerHTML = "Saisie non valide. Caractère non autorisé."
        errorMsg.style.color = "rgb(180, 0, 0)"
    }
    return testAddress
}

// ************** VALIDATION VILLE **************
const validCity = () => {
    const inputCity = form.city

    // création de la reg exp pour la validation de la ville
    let cityRegExp = new RegExp("^[a-zA-Z-'\é\è\ê\ï\î]+$", 'g')
    let testCity = cityRegExp.test(inputCity.value)

    // Récupération de la balise suivant l'input, soit le "p" pour y afficher le message
    let errorMsg = inputCity.nextElementSibling

    // on test le reg exp
    if (testCity) {
        errorMsg.innerHTML = "Saisie valide"
        errorMsg.style.color = "rgb(0, 220, 0)"
    } else {
        errorMsg.innerHTML = "Saisie non valide. Caractère non autorisé."
        errorMsg.style.color = "rgb(180, 0, 0)"
    }
    return testCity
}

// ************** VALIDATION EMAIL **************
const validEmail = () => {
    const inputEmail = form.email

    // création de la reg exp pour la validation d'email
    let emailRegExp = new RegExp('^[a-zA-Z0-9.-_]+[@]{1}[a-zA-Z0-9.-_]+[.]{1}([a-z]{2,3})+$', 'g')

    let testEmail = emailRegExp.test(inputEmail.value)

    // Récupération de la balise suivant l'input, soit le "p" pour y afficher le message
    let errorMsg = inputEmail.nextElementSibling
    // console.log(testEmail);

    // on test le reg exp
    if (testEmail) {
        errorMsg.innerHTML = "Adresse mail valide"
        errorMsg.style.color = "rgb(0, 220, 0)"
    } else {
        errorMsg.innerHTML = "Adresse mail non valide. Voici un exemple : test@domaine.fr"
        errorMsg.style.color = "rgb(180, 0, 0)"
    }
    return testEmail
}


//////////////////////////////////////////////////////////////////////////////////
// ********** BOUTTON COMMANDE **********
const orderButton = document.getElementById("order")

// Dès qu'on clique sur le boutton Commander, on doit d'abord vérifier les champs
const formCheck = () => {
    return cartCheck() &&
        validFirstName() &&
        validLastName() &&
        validAddress() &&
        validCity() &&
        validEmail()

}


// On vérifie si le panier est vide ou non. Si vide, alors renvois vers INDEX + message alert "Votre panier est vide"
const cartCheck = () => {
    const cart = getCart()
    if (cart.length < 1) {
        alert("Votre panier est vide. Veuillez sélectionner un ou plusieurs articles.")
        return false
    }
    return true
}


// On envoie les infos dans une requête fetch POST
orderButton.addEventListener("click", (e) => {
    e.preventDefault()

    // on créé l'objet contact en allant chercher les valeurs saisie dans le formulaire
    const contact = {
        firstName: form.firstName.value,
        lastName: form.lastName.value,
        address: form.address.value,
        city: form.city.value,
        email: form.email.value
    }

    // on créé le tableau products en allant récupérer que l'ID des produits présent dans le LS
    const products = getCart().map(product => product._id)


    // Si notre formulaire est saisie correctement, alors on peut envoyer les données a l'API
    if (formCheck()) {
        fetch('http://localhost:3000/api/products/order', {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    contact,
                    products
                })
            })
            .then(response => response.json())
            .then(data => {
                JSON.stringify(data)
                window.location.href = `./confirmation.html?order=${data.orderId}`

            })
            .catch(error => console.log(error))
        // Si les champs sont pas "OK"
        // Afficher un message d'erreur indiquant à l'utilisateur de vérifier la saisie de son formulaire
    }
})