// Fonction pour enregistrer le panier
const saveCart = (cart) => {
    localStorage.setItem("cart", JSON.stringify(cart))
}

// function pour sélectionner le panier
const getCart = () => {
    let cart = localStorage.getItem("cart")
    return cart == null ? [] : JSON.parse(cart)
}


// Ajout du produit au panier
const addCart = (product, button) => {
    const cart = getCart()
    const quantity = parseInt(document.getElementById('quantity').value)
    const color = document.getElementById('colors').value

    // Si la quantité ou couleur sont pas remplis, on peut pas ajouter le produit
    if (quantity < 1 || quantity > 100 || color === "") {
        alert("Veuillez choisir une quantité entre 1 et 100 et/ou une couleur")
    } else {
        const foundProduct = cart.find(p => p._id === product._id && p.color === color)

        if (foundProduct != undefined) {
            foundProduct.quantity += quantity
        } else {
            const item = {
                _id: product._id,
                color,
                quantity
            }
            cart.push(item)
        }
        saveCart(cart)
        button.innerText = "Produit ajouté !"
        button.style.color = "rgb(0, 205, 0)"
    }
}



// Suppression d'un produit du panier
const removeFromCart = (product) => {
    const cart = getCart()
    const updatedCart = cart.filter(p => !(p._id === product._id && p.color === product.color))
    saveCart(updatedCart)
}

// changer la quantité d'un produit depuis le panier
const changeQuantity = (cart, product, quantity) => {
    cart.forEach(p => {
        if (p._id === product._id && p.color === product.color) {
            p.quantity = quantity
        }
    })
    saveCart(cart)
}


// on créé une fonction qui va permettre de généraliser le Search Params
const getParamUrl = (param) => {
    const url = document.URL
    paramsString = url.split("?")[1]
    const searchParams = new URLSearchParams(paramsString)
    return searchParams.get(param)
}