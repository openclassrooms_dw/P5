fetch('http://localhost:3000/api/products')
    .then(response => response.json())
    .then(products => {
        const item = document.getElementById("items")
        products.forEach(product => {
            const a = document.createElement("a")
            a.href = `./product.html?id=${product._id}`

            const article = document.createElement("article")

            const img = document.createElement("img")
            img.src = product.imageUrl
            img.alt = product.altTxt

            const h3 = document.createElement("h3")
            h3.classList.add("productName")
            h3.innerText = product.name

            const p = document.createElement("p")
            p.classList.add("productDescription")
            p.innerText = product.description

            article.append(img)
            article.append(h3)
            article.append(p)
            a.append(article)
            item.append(a)
        });
    })