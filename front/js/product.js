// On va chercher l'url afin d'extraire l'id présent dedans et l'appeler sur notre nouvelle page pour qu'il affiche le bon produit
const productId = getParamUrl("id")

fetch(`http://localhost:3000/api/products/${productId}`)
    .then(response => response.json())
    .then(product => {
        // Ajouter l'image du produit
        const itemImg = document.querySelector(".item__img")
        const img = document.createElement("img")
        img.src = product.imageUrl
        img.alt = product.altTxt
        itemImg.append(img)

        // Ajouter le nom du produit
        const itemName = document.getElementById("title")
        itemName.innerText = product.name

        // Ajouter le prix de produit
        const itemPrice = document.getElementById("price")
        itemPrice.innerText = product.price

        // Ajouter la description du produit
        const itemDescription = document.getElementById("description")
        itemDescription.innerText = product.description

        // Ajouter les options
        const select = document.getElementById("colors")

        // Créé une boucle qui va aller chercher chaque couleurs
        product.colors.forEach(color => {
            const option = document.createElement("option")
            option.value = color
            option.innerText = color

            select.append(option)

        })
        // On récupère l'élément sur lequel on veut détecter le clic
        const button = document.getElementById("addToCart")

        // On écoute l'evenement et on modifie le texte afficher
        button.addEventListener('click', () => addCart(product, button))
    })